import cv2
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as clr

def VideoToArray(path):
    cap = cv2.VideoCapture(path)
    frameCount = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    frameWidth = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    frameHeight = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

    buf = np.empty((frameCount, frameHeight, frameWidth, 3), np.dtype('uint8'))

    fc = 0
    ret = True

    while (fc < frameCount and ret):
        ret, buf[fc] = cap.read()
        fc += 1

    cap.release()

    return buf
i=input("Please enter your file name:  ")
vid = VideoToArray(i)

frameCount = len(vid)
frameHeight = len(vid[0])
frameWidth = len(vid[0][0])

pixelsPerFrame = frameHeight * frameWidth

avgRGB = np.empty((frameCount,3))
avgHSV = np.empty((frameCount,3))

for frameIndex in range(frameCount):

    avgRGB[frameIndex,0] = np.sum(vid[frameIndex, 0:frameHeight, 0:frameWidth, 0]) / pixelsPerFrame
    avgRGB[frameIndex,1] = np.sum(vid[frameIndex, 0:frameHeight, 0:frameWidth, 1]) / pixelsPerFrame
    avgRGB[frameIndex,2] = np.sum(vid[frameIndex, 0:frameHeight, 0:frameWidth, 2]) / pixelsPerFrame

    print(frameIndex, '\n')

avgHSV = clr.rgb_to_hsv(avgRGB)

fig, axs = plt.subplots(2, 2)

axs[0, 0].plot(range(frameCount), avgRGB[0:frameCount,2], color = 'red', label = "Red")
axs[0, 0].plot(range(frameCount), avgRGB[0:frameCount,1], color = 'green', label = "Green")
axs[0, 0].plot(range(frameCount), avgRGB[0:frameCount,0], color = 'blue', label = "Blue")
axs[0, 0].set_title("RGB")

axs[1, 0].plot(range(frameCount), avgHSV[0:frameCount,0], color = 'magenta')
axs[1, 0].set_title("Hue")

axs[0, 1].plot(range(frameCount), avgHSV[0:frameCount,1], color = 'red')
axs[0, 1].set_title("Saturation")

axs[1, 1].plot(range(frameCount), avgHSV[0:frameCount,2], color ='gray')
axs[1, 1].set_title("Value")


plt.show()
